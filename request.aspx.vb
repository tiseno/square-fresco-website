Imports System.Net.Mail
Imports System.Text.RegularExpressions

Partial Class request
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim requestname1 As String = Replace(request("requestname"), "'", "''")
            Dim requestcompany As String = Replace(request("requestcompany"), "'", "''")
            Dim requestemail As String = Replace(request("requestemail"), "'", "''")
            Dim requesttel As String = Replace(request("requesttel"), "'", "''")
            Dim requestknowmore As String = Replace(request("requestknowmore"), "'", "''")

            Dim requestbudget As String = Replace(request("requestbudget"), "'", "''")
            Dim requesttime As String = Replace(request("requesttime"), "'", "''")
            Dim requestpurpose As String = Replace(request("requestpurpose"), "'", "''")
            Dim requestbrief As String = Replace(request("requestbrief"), "'", "''")


            Const ToAddress As String = "info@squarefresco.com"
            Dim mm As New MailMessage()
            mm.Subject = "Customer Request for Quote Squarefresco - " & requestname1
            mm.To.Add(ToAddress)
            mm.From = New MailAddress("info@squarefresco.com")
            Dim body As String = "Company Name: " & requestcompany & vbCrLf
            body += "Contact No: " & requesttel & vbCrLf
            body += "Email: " & requestemail & vbCrLf
            body += "To know more: " & requestknowmore & vbCrLf

            body += "Budget: " & requestbudget & vbCrLf
            body += "project Time: " & requesttime & vbCrLf
            body += "Purpose: " & requestpurpose & vbCrLf
            body += "Brief: " & requestbrief

            mm.Body = body
            mm.IsBodyHtml = False

            Dim smtp As New SmtpClient("mail.squarefresco.com", 25)
            smtp.Send(mm)
            Response.Write("<SCR" + "IPT language='javascript'>alert('Thank you for the request, we will request you soon.');</SCR" + "IPT>")

        Catch ex As Exception
            Response.Write("<SCR" + "IPT language='javascript'>alert('Sorry, System Error, please send your email to info@squarefresco.com');</SCR" + "IPT>")
        End Try

        Dim requestname As String = request("requestname")

        'If captexName <> "" Then
        '    Const ToAddress As String = "info@quality-direct.net"
        '    Dim mm As New MailMessage()
        '    mm.Subject = "Neneral Enquiry from Q-Website"
        '    mm.To.Add(ToAddress)
        '    mm.From = New MailAddress("info@quality-direct.net")
        '    Dim body As String = "Company Name: " & captexName & vbCrLf
        '    body += "request No: " & captexTel & vbCrLf
        '    body += "Email: " & captexEmail & vbCrLf
        '    body += "Domain Name: " & captexDomain & vbCrLf
        '    body += "Product: " & captexProduct & vbCrLf
        '    body += "Expected time: " & capddlTime & vbCrLf
        '    body += "Purpose: " & capddlWebPurpose & vbCrLf
        '    body += "Remark: " & captexComment

        '    mm.Body = body
        '    mm.IsBodyHtml = False

        '    Dim smtp As New SmtpClient("mail.quality-direct.net", 25)
        '    smtp.Send(mm)
        '    Response.Write("<SCR" + "IPT language='javascript'>alert('Thank you for your enquiry, We will revert to your soon!') ;window.location='enquiry_form.html';</SCR" + "IPT>")
        'Else
        '    Response.Write("<SCR" + "IPT language='javascript'>alert('Sorry, kindly fill in your company Name and request No.') ;window.location='enquiry_form.html';</SCR" + "IPT>")

        'End If

    End Sub
End Class
