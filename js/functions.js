// JavaScript Document

/* ---------------------------------- floating menu --------------- */
/** expandable menu, must place at the end before body closing */

//config
$float_speed=1000; //milliseconds
$float_easing="easeOutQuint";
$menu_fade_speed=300; //milliseconds
$page_load_fade_delay=1000; //milliseconds
$closed_menu_opacity=1;

//cache vars
$sfresco_menu=$("#sfresco_menu");
$sfresco_menu_menu=$("#sfresco_menu .menu");
$sfresco_menu_label=$("#sfresco_menu .label");
$sfresco_menu_item=$("#sfresco_menu .menu .menu_item");

$(window).load(function() {
	menuPosition=$sfresco_menu.position().top;
	FloatMenu();
	$sfresco_menu_item.delay($page_load_fade_delay).fadeTo($menu_fade_speed, 0, function(){$sfresco_menu_item.css("display","none");});
	$sfresco_menu.hover(
		function(){ //mouse over
			$sfresco_menu_label.stop().fadeTo($menu_fade_speed, 1);
			$sfresco_menu_item.css("display","block").stop().fadeTo($menu_fade_speed, 1);
		},
		function(){ //mouse out
			$sfresco_menu_label.stop().fadeTo($menu_fade_speed, $closed_menu_opacity, function(){$sfresco_menu_item.css("display","none");});
			$sfresco_menu_item.stop().fadeTo($menu_fade_speed, 0);
		}
	);
});

$(window).scroll(function () { 
	FloatMenu();
});

function FloatMenu(){
	var scrollAmount=$(document).scrollTop();
	var newPosition=menuPosition+scrollAmount;
	if($(window).height()<$sfresco_menu.height()+$sfresco_menu_menu.height()){
		$sfresco_menu.css("top",menuPosition);
	} else {
		$sfresco_menu.stop().animate({top: newPosition}, $float_speed, $float_easing);
	}
}

/* ---------------------------------- smooth scrolling --------------- */

            $(function() {
                $('#sfresco_menu .menu a, .back-to-top a, .footer-container .footer-wrapper .footer-left ul li a, .about-banner-request-quote a').bind('click',function(event){
                    var $anchor = $(this);
                    
                    $('html, body').stop().animate({
                        scrollTop: $($anchor.attr('href')).offset().top
                    }, 1500,'easeInOutExpo');
                    /*
                    if you don't want to use the easing effects:
                    $('html, body').stop().animate({
                        scrollTop: $($anchor.attr('href')).offset().top
                    }, 1000);
                    */
                    event.preventDefault();
                });
            });
												
												
/* ---------------------------------- pop up modal --------------- */



$(document).ready(function() {	

	//select all the a tag with name equal to modal
	$('a[name=modal]').click(function(e) {
		//Cancel the link behavior
		e.preventDefault();
		
		//Get the A tag
		var id = $(this).attr('href');
	
		//Get the screen height and width
		var maskHeight = $(document).height();
		var maskWidth = $(window).width();
	
		//Set heigth and width to mask to fill up the whole screen
		$('#mask').css({'width':maskWidth,'height':maskHeight});
		
		//transition effect		
		$('#mask').fadeIn(300);	
		$('#mask').fadeTo("fast",0.8);	
	
		//Get the window height and width
		var winH = $(window).height();
		var winW = $(window).width();
              
		//Set the popup window to center
		$(id).css('top',  winH/2-$(id).height()/2);
		$(id).css('left', winW/2-$(id).width()/2);
	
		//transition effect
		$(id).fadeIn(800); 
	
	});
	
	//if close button is clicked
	$('.window .close').click(function (e) {
		//Cancel the link behavior
		e.preventDefault();
		
		$('#mask').hide();
		$('.window').hide();
	});		
	
	//if mask is clicked
	$('#mask').click(function () {
/*		$(this).hide();
		$('.window').hide();
*/	});			
	
});


