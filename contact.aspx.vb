Imports System.Net.Mail
Imports System.Text.RegularExpressions

Partial Class Contact
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim contactname1 As String = Replace(Request("contactname"), "'", "''")
            Dim contactcompany As String = Replace(Request("contactcompany"), "'", "''")
            Dim contactemail As String = Replace(Request("contactemail"), "'", "''")
            Dim contacttel As String = Replace(Request("contacttel"), "'", "''")
            Dim contactknowmore As String = Replace(Request("contactknowmore"), "'", "''")

            Const ToAddress As String = "info@squarefresco.com"
            Dim mm As New MailMessage()
            mm.Subject = "Customer Enquiry Squarefresco - " & contactname1
            mm.To.Add(ToAddress)
            mm.From = New MailAddress("info@squarefresco.com")
            Dim body As String = "Company Name: " & contactcompany & vbCrLf
            body += "Contact No: " & contacttel & vbCrLf
            body += "Email: " & contactemail & vbCrLf
            body += "Remark: " & contactknowmore

            mm.Body = body
            mm.IsBodyHtml = False

            Dim smtp As New SmtpClient("mail.squarefresco.com", 25)
            smtp.Send(mm)
            Response.Write("<SCR" + "IPT language='javascript'>alert('Thank you for the enquiry, we will contact you soon.');</SCR" + "IPT>")

        Catch ex As Exception
            Response.Write("<SCR" + "IPT language='javascript'>alert('Sorry, System Error, please send your email to info@squarefresco.com');</SCR" + "IPT>")
        End Try

        Dim contactname As String = Request("contactname")

        'If captexName <> "" Then
        '    Const ToAddress As String = "info@quality-direct.net"
        '    Dim mm As New MailMessage()
        '    mm.Subject = "Neneral Enquiry from Q-Website"
        '    mm.To.Add(ToAddress)
        '    mm.From = New MailAddress("info@quality-direct.net")
        '    Dim body As String = "Company Name: " & captexName & vbCrLf
        '    body += "Contact No: " & captexTel & vbCrLf
        '    body += "Email: " & captexEmail & vbCrLf
        '    body += "Domain Name: " & captexDomain & vbCrLf
        '    body += "Product: " & captexProduct & vbCrLf
        '    body += "Expected time: " & capddlTime & vbCrLf
        '    body += "Purpose: " & capddlWebPurpose & vbCrLf
        '    body += "Remark: " & captexComment

        '    mm.Body = body
        '    mm.IsBodyHtml = False

        '    Dim smtp As New SmtpClient("mail.quality-direct.net", 25)
        '    smtp.Send(mm)
        '    Response.Write("<SCR" + "IPT language='javascript'>alert('Thank you for your enquiry, We will revert to your soon!') ;window.location='enquiry_form.html';</SCR" + "IPT>")
        'Else
        '    Response.Write("<SCR" + "IPT language='javascript'>alert('Sorry, kindly fill in your company Name and contact No.') ;window.location='enquiry_form.html';</SCR" + "IPT>")

        'End If

    End Sub
End Class
